package et.gov.customs.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class CnConfigServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(CnConfigServerApplication.class, args);
  }

}
